require "sinatra"
set :bind, "0.0.0.0"
set :port, 5678

get '/time' do
  p Time.now
end
get "/hello" do
  "hello"
end
get '/sum' do
  a = params['a'].to_i
  b = params['b'].to_i
  a + b
end
